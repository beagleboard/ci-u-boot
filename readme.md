# CI Builds

https://beagleboard.beagleboard.io/ci-u-boot/

# BeagleBoneAI64 microSD

```
wget https://beagleboard.beagleboard.io/ci-u-boot/beagleboneai64/get_n_install_beagleboneai64_microSD.sh
chmod +x ./get_n_install_beagleboneai64_microSD.sh
sudo ./get_n_install_beagleboneai64_microSD.sh
```

# BeagleBoneAI64 eMMC

```
wget https://beagleboard.beagleboard.io/ci-u-boot/beagleboneai64/get_n_install_beagleboneai64_eMMC.sh
chmod +x ./get_n_install_beagleboneai64_eMMC.sh
sudo ./get_n_install_beagleboneai64_eMMC.sh
```

# BeaglePlay microSD

```
wget https://beagleboard.beagleboard.io/ci-u-boot/beagleplay/get_n_install_beagleplay_microSD.sh
chmod +x ./get_n_install_beagleplay_microSD.sh
sudo ./get_n_install_beagleplay_microSD.sh
```

# BeaglePlay eMMC

```
wget https://beagleboard.beagleboard.io/ci-u-boot/beagleplay/get_n_install_beagleplay_eMMC.sh
chmod +x ./get_n_install_beagleplay_eMMC.sh
sudo ./get_n_install_beagleplay_eMMC.sh
```
