#!/bin/bash

if ! id | grep -q root; then
	echo "must be run as root"
	exit
fi

wget https://beagleboard.beagleboard.io/ci-u-boot/beagleboneai64/sysfw.itb
wget https://beagleboard.beagleboard.io/ci-u-boot/beagleboneai64/tiboot3.bin
wget https://beagleboard.beagleboard.io/ci-u-boot/beagleboneai64/tispl.bin
wget https://beagleboard.beagleboard.io/ci-u-boot/beagleboneai64/u-boot.img

if [ -d /boot/firmware/ ] ; then
	cp -v ./sysfw.itb /boot/firmware/
	cp -v ./tiboot3.bin /boot/firmware/
	cp -v ./tispl.bin /boot/firmware/
	cp -v ./u-boot.img /boot/firmware/
	sync
fi

if [ -b /dev/mmcblk0 ] ; then
	mmc bootpart enable 1 2 /dev/mmcblk0
	mmc bootbus set single_backward x1 x8 /dev/mmcblk0
	mmc hwreset enable /dev/mmcblk0

	echo "Clearing eMMC boot0"

	echo '0' >> /sys/class/block/mmcblk0boot0/force_ro

	echo "dd if=/dev/zero of=/dev/mmcblk0boot0 count=32 bs=128k"
	sudo dd if=/dev/zero of=/dev/mmcblk0boot0 count=32 bs=128k
	sync
fi
