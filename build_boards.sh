#!/bin/bash

CC32=arm-linux-gnueabihf-
CC64=aarch64-linux-gnu-

${CC32}gcc --version
${CC64}gcc --version

git --version

DIR=$PWD

#ti-linux-firmware
#HEAD: https://git.ti.com/gitweb?p=processor-firmware/ti-linux-firmware.git;a=summary
TILF_DIR=ti-linux-firmware
TILF_REPO="https://git.ti.com/git/processor-firmware/ti-linux-firmware.git"
TILF_REPO_LOCAL="https://git.gfnd.rcn-ee.org/TexasInstruments/ti-linux-firmware.git"

#trusted-firmware-a
#HEAD: https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/
TFA_DIR=trusted-firmware-a
TFA_REPO="https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git"
TFA_REPO_LOCAL="https://git.gfnd.rcn-ee.org/mirror/trusted-firmware-a.git"

#optee_os
#HEAD: https://github.com/OP-TEE/optee_os
OPTEE_DIR=optee_os
OPTEE_REPO="https://github.com/OP-TEE/optee_os.git"
OPTEE_REPO_LOCAL="https://git.gfnd.rcn-ee.org/mirror/optee_os.git"

configure_board () {
	#U-Boot
	#HEAD: https://github.com/u-boot/u-boot
	UB_REPO="https://github.com/u-boot/u-boot.git"
	UB_REPO_LOCAL="https://git.gfnd.rcn-ee.org/mirror/u-boot.git"

	TILF_TAG=ti-linux-firmware
	TFA_TAG=main
	unset TFA_EXTRA_ARGS
	OPTEE_TAG=master
	unset OPTEE_EXTRA_ARGS
	UB_TAG=master

	case "${DEVICE}" in
	am69)
		UB_TAG=next

		SOC_NAME=j784s4
		SECURITY_TYPE=hs-fs

		TFA_BOARD=j784s4
		TFA_EXTRA_ARGS="K3_USART=0x8"

		OPTEE_PLATFORM=k3-j784s4
		OPTEE_EXTRA_ARGS="CFG_CONSOLE_UART=0x8"

		UBOOT_CFG_CORTEXR="am69_sk_r5_defconfig"
		UBOOT_CFG_CORTEXA="am69_sk_a72_defconfig"
		;;
	beagleboneai64)
		UB_TAG=next

		SOC_NAME=j721e
		SECURITY_TYPE=gp

		TFA_BOARD=generic

		OPTEE_PLATFORM=k3-j721e

		UBOOT_CFG_CORTEXR="j721e_beagleboneai64_r5_defconfig"
		UBOOT_CFG_CORTEXA="j721e_beagleboneai64_a72_defconfig"
		;;
	beagleplay)
		UB_TAG=next

		SOC_NAME=am62x
		SECURITY_TYPE=gp

		TFA_BOARD=lite

		OPTEE_PLATFORM=k3-am62x
		OPTEE_EXTRA_ARGS="CFG_WITH_SOFTWARE_PRNG=y"

		UBOOT_CFG_CORTEXR="am62x_beagleplay_r5_defconfig"
		UBOOT_CFG_CORTEXA="am62x_beagleplay_a53_defconfig"
		;;
	j722s)
		UB_TAG=next

		SOC_NAME=j722s
		SECURITY_TYPE=hs-fs

		TFA_BOARD=lite

		OPTEE_PLATFORM=k3-am62x
		OPTEE_EXTRA_ARGS="CFG_WITH_SOFTWARE_PRNG=y CFG_TEE_CORE_LOG_LEVEL=1"

		UBOOT_CFG_CORTEXR="j722s_evm_r5_defconfig"
		UBOOT_CFG_CORTEXA="j722s_evm_a53_defconfig"
		;;
	esac
}

clone_repos () {
	if [ -d ${DIR}/public/${DEVICE}/ ] ; then
		rm -rf ${DIR}/public/${DEVICE}/ || true
	fi
	mkdir -p ${DIR}/public/${DEVICE}/

	if [ -d ${DIR}/src/${DEVICE}/${TILF_DIR}/ ] ; then
		rm -rf ${DIR}/src/${DEVICE}/${TILF_DIR}/ || true
	else
		mkdir -p ${DIR}/src/${DEVICE}/${TILF_DIR}/
	fi

	if [ -d ${DIR}/src/${DEVICE}/${TFA_DIR}/ ] ; then
		rm -rf ${DIR}/src/${DEVICE}/${TFA_DIR}/ || true
	else
		mkdir -p ${DIR}/src/${DEVICE}/${TFA_DIR}/
	fi

	if [ -d ${DIR}/src/${DEVICE}/${OPTEE_DIR}/ ] ; then
		rm -rf ${DIR}/src/${DEVICE}/${OPTEE_DIR}/ || true
	else
		mkdir -p ${DIR}/src/${DEVICE}/${OPTEE_DIR}/
	fi

	if [ -d ${DIR}/src/${DEVICE}/u-boot/ ] ; then
		rm -rf ${DIR}/src/${DEVICE}/u-boot/ || true
	else
		mkdir -p ${DIR}/src/${DEVICE}/u-boot/
	fi

	if [ -f .localgit ] ; then
		echo "Using Local Git Mirror"
		git -c http.sslVerify=false clone -b ${TILF_TAG} ${TILF_REPO_LOCAL} --depth=5 ${DIR}/src/${DEVICE}/${TILF_DIR}/
		git -c http.sslVerify=false clone -b ${TFA_TAG} ${TFA_REPO_LOCAL} --depth=5 ${DIR}/src/${DEVICE}/${TFA_DIR}/
		git -c http.sslVerify=false clone -b ${OPTEE_TAG} ${OPTEE_REPO_LOCAL} --depth=5 ${DIR}/src/${DEVICE}/${OPTEE_DIR}/
		git -c http.sslVerify=false clone -b ${UB_TAG} ${UB_REPO_LOCAL} --depth=5 ${DIR}/src/${DEVICE}/u-boot/
	else
		git clone -b ${TILF_TAG} ${TILF_REPO} --depth=5 ${DIR}/src/${DEVICE}/${TILF_DIR}/
		git clone -b ${TFA_TAG} ${TFA_REPO} --depth=5 ${DIR}/src/${DEVICE}/${TFA_DIR}/
		git clone -b ${OPTEE_TAG} ${OPTEE_REPO} --depth=5 ${DIR}/src/${DEVICE}/${OPTEE_DIR}/
		git clone -b ${UB_TAG} ${UB_REPO} --depth=5 ${DIR}/src/${DEVICE}/u-boot/
	fi

	if [ -d ${DIR}/src/${DEVICE}/${TILF_DIR}/.git/ ] ; then
		echo "ti-linux-firmware: BRANCH=$TILF_TAG" > ${DIR}/public/${DEVICE}/changes.txt
		git --git-dir ${DIR}/src/${DEVICE}/${TILF_DIR}/.git log --pretty=oneline >> ${DIR}/public/${DEVICE}/changes.txt
	else
		echo "ti-linux-firmware: GIT FAILURE" > ${DIR}/public/${DEVICE}/changes.txt
	fi

	if [ -d ${DIR}/src/${DEVICE}/${TFA_DIR}/.git/ ] ; then
		echo "trusted-firmware-a: BRANCH=$TFA_TAG" >> ${DIR}/public/${DEVICE}/changes.txt
		git --git-dir ${DIR}/src/${DEVICE}/${TFA_DIR}/.git log --pretty=oneline >> ${DIR}/public/${DEVICE}/changes.txt
	else
		echo "trusted-firmware-a: GIT FAILURE" >> ${DIR}/public/${DEVICE}/changes.txt
	fi

	if [ -d ${DIR}/src/${DEVICE}/${OPTEE_DIR}/.git/ ] ; then
		echo "optee_os: BRANCH=$OPTEE_TAG" >> ${DIR}/public/${DEVICE}/changes.txt
		git --git-dir ${DIR}/src/${DEVICE}/${OPTEE_DIR}/.git log --pretty=oneline >> ${DIR}/public/${DEVICE}/changes.txt
	else
		echo "optee_os: GIT FAILURE" >> ${DIR}/public/${DEVICE}/changes.txt
	fi

	if [ -d ${DIR}/src/${DEVICE}/u-boot/.git/ ] ; then
		echo "u-boot: BRANCH=$UB_TAG" >> ${DIR}/public/${DEVICE}/changes.txt
		git --git-dir ${DIR}/src/${DEVICE}/u-boot/.git log --pretty=oneline >> ${DIR}/public/${DEVICE}/changes.txt
	else
		echo "u-boot: GIT FAILURE" >> ${DIR}/public/${DEVICE}/changes.txt
	fi
}

build_board () {
	echo "make -C ${DIR}/src/${DEVICE}/${TFA_DIR}/ -j4 CROSS_COMPILE=$CC64 ARCH=aarch64 PLAT=k3 SPD=opteed $TFA_EXTRA_ARGS TARGET_BOARD=${TFA_BOARD} all"
	#make -C ${DIR}/src/${DEVICE}/${TFA_DIR}/ -j4 CROSS_COMPILE=$CC64 CFLAGS= LDFLAGS= ARCH=aarch64 PLAT=k3 SPD=opteed $TFA_EXTRA_ARGS TARGET_BOARD=${TFA_BOARD} all
	make -C ${DIR}/src/${DEVICE}/${TFA_DIR}/ -j4 CROSS_COMPILE=$CC64 ARCH=aarch64 PLAT=k3 SPD=opteed $TFA_EXTRA_ARGS TARGET_BOARD=${TFA_BOARD} all

	if [ ! -f ${DIR}/src/${DEVICE}/${TFA_DIR}/build/k3/${TFA_BOARD}/release/bl31.bin ] ; then
		echo "Failure in ${TFA_DIR}"
	else
		cp -v ${DIR}/src/${DEVICE}/${TFA_DIR}/build/k3/${TFA_BOARD}/release/bl31.bin ${DIR}/public/${DEVICE}/
	fi

	echo "make -C ${DIR}/src/${DEVICE}/${OPTEE_DIR}/ -j4 O=../optee CROSS_COMPILE=$CC32 CROSS_COMPILE64=$CC64 CFLAGS= LDFLAGS= CFG_ARM64_core=y $OPTEE_EXTRA_ARGS PLATFORM=${OPTEE_PLATFORM} all"
	make -C ${DIR}/src/${DEVICE}/${OPTEE_DIR}/ -j4 O=../optee CROSS_COMPILE=$CC32 CROSS_COMPILE64=$CC64 CFLAGS= LDFLAGS= CFG_ARM64_core=y $OPTEE_EXTRA_ARGS PLATFORM=${OPTEE_PLATFORM} all

	if [ ! -f ${DIR}/src/${DEVICE}/optee/core/tee-pager_v2.bin ] ; then
		echo "Failure in ${OPTEE_DIR}"
	else
		cp -v ${DIR}/src/${DEVICE}/optee/core/tee-pager_v2.bin ${DIR}/public/${DEVICE}/
	fi

	echo "make -C ${DIR}/src/${DEVICE}/u-boot/ -j1 O=../CORTEXR CROSS_COMPILE=$CC32 $UBOOT_CFG_CORTEXR"
	make -C ${DIR}/src/${DEVICE}/u-boot/ -j1 O=../CORTEXR CROSS_COMPILE=$CC32 $UBOOT_CFG_CORTEXR

	echo "make -C ${DIR}/src/${DEVICE}/u-boot/ -j4 O=../CORTEXR CROSS_COMPILE=$CC32 BINMAN_INDIRS=${DIR}/src/${DEVICE}/${TILF_DIR}/"
	make -C ${DIR}/src/${DEVICE}/u-boot/ -j4 O=../CORTEXR CROSS_COMPILE=$CC32 BINMAN_INDIRS=${DIR}/src/${DEVICE}/${TILF_DIR}/

	if [ ! -f ${DIR}/src/${DEVICE}/CORTEXR/tiboot3-${SOC_NAME}-${SECURITY_TYPE}-evm.bin ] ; then
		echo "Failure in u-boot $UBOOT_CFG_CORTEXR"
	else
		cp -v ${DIR}/src/${DEVICE}/CORTEXR/tiboot3-${SOC_NAME}-${SECURITY_TYPE}-evm.bin ${DIR}/public/${DEVICE}/tiboot3.bin
		if [ -f ${DIR}/src/${DEVICE}/CORTEXR/sysfw-${SOC_NAME}-${SECURITY_TYPE}-evm.itb ] ; then
			cp -v ${DIR}/src/${DEVICE}/CORTEXR/sysfw-${SOC_NAME}-${SECURITY_TYPE}-evm.itb ${DIR}/public/${DEVICE}/sysfw.itb
		fi
	fi

	if [ -f ${DIR}/public/${DEVICE}/bl31.bin ] ; then
		if [ -f ${DIR}/public/${DEVICE}/tee-pager_v2.bin ] ; then
			echo "make -C ${DIR}/src/${DEVICE}/u-boot/ -j1 O=../CORTEXA CROSS_COMPILE=$CC64 $UBOOT_CFG_CORTEXA"
			make -C ${DIR}/src/${DEVICE}/u-boot/ -j1 O=../CORTEXA CROSS_COMPILE=$CC64 $UBOOT_CFG_CORTEXA

			echo "make -C ${DIR}/src/${DEVICE}/u-boot/ -j4 O=../CORTEXA CROSS_COMPILE=$CC64 BL31=${DIR}/public/${DEVICE}/bl31.bin TEE=${DIR}/public/${DEVICE}/tee-pager_v2.bin BINMAN_INDIRS=${DIR}/src/${DEVICE}/${TILF_DIR}/"
			make -C ${DIR}/src/${DEVICE}/u-boot/ -j4 O=../CORTEXA CROSS_COMPILE=$CC64 BL31=${DIR}/public/${DEVICE}/bl31.bin TEE=${DIR}/public/${DEVICE}/tee-pager_v2.bin BINMAN_INDIRS=${DIR}/src/${DEVICE}/${TILF_DIR}/

			case "${SECURITY_TYPE}" in
			gp)
				if [ ! -f ${DIR}/src/${DEVICE}/CORTEXA/tispl.bin_unsigned ] ; then
					echo "Failure in u-boot $UBOOT_CFG_CORTEXA"
				else
					cp -v ${DIR}/src/${DEVICE}/CORTEXA/tispl.bin_unsigned ${DIR}/public/${DEVICE}/tispl.bin || true
					cp -v ${DIR}/src/${DEVICE}/CORTEXA/u-boot.img_unsigned ${DIR}/public/${DEVICE}/u-boot.img || true
				fi
				;;
			hs-fs)
				if [ ! -f ${DIR}/src/${DEVICE}/CORTEXA/tispl.bin ] ; then
					echo "Failure in u-boot $UBOOT_CFG_CORTEXA"
				else
					cp -v ${DIR}/src/${DEVICE}/CORTEXA/tispl.bin ${DIR}/public/${DEVICE}/tispl.bin || true
					cp -v ${DIR}/src/${DEVICE}/CORTEXA/u-boot.img ${DIR}/public/${DEVICE}/u-boot.img || true
				fi
				;;
			esac
		else
			echo "Missing ${DIR}/public/${DEVICE}/tee-pager_v2.bin"
		fi
	else
		echo "Missing ${DIR}/public/${DEVICE}/bl31.bin"
	fi

	tree -sh ${DIR}/public/${DEVICE}/
}

copy_install_script () {
	cp -v ${DIR}/get_n_install_${DEVICE}_microSD.sh ${DIR}/public/${DEVICE}/
	cp -v ${DIR}/get_n_install_${DEVICE}_eMMC.sh ${DIR}/public/${DEVICE}/
}

DEVICE="am69" ; configure_board
DEVICE="am69" ; clone_repos
DEVICE="am69" ; build_board

DEVICE="beagleboneai64" ; configure_board
DEVICE="beagleboneai64" ; clone_repos
DEVICE="beagleboneai64" ; build_board
DEVICE="beagleboneai64" ; copy_install_script

DEVICE="beagleplay" ; configure_board
DEVICE="beagleplay" ; clone_repos
DEVICE="beagleplay" ; build_board
DEVICE="beagleplay" ; copy_install_script

DEVICE="j722s" ; configure_board
DEVICE="j722s" ; clone_repos
DEVICE="j722s" ; build_board

#
